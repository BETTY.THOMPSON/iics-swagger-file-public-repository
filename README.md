# IICS Swagger File Public Repository

This repository's purpose is to store the various Swagger 2.0 files that CDI uses in its various Business Service connections. More information on this process (in the context of PersonAPI) can be found [In This Document](https://git.doit.wisc.edu/interop/iics/external-iics-docs/-/blob/master/docs/tutorials/PersonAPI/PersonAPIwRESTv2.md).
